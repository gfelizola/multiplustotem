package scenes
{
	import com.greensock.TweenMax;
	
	import events.SceneEvent;
	
	import game.ParceiroHome;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	public class HomeScene extends Scene
	{
		private var toqueImg:Image;
		private var toqueTxt:TextField;
		
		private var comeceTxt:TextField;
		private var btnIncio:Button;
		private var parceiro:ParceiroHome;
		
		public function HomeScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event = null):void
		{
			super.create(e);
			
			comeceTxt = new TextField(600, 300, "COMECE A\nGANHAR PONTOS\nAGORA MESMO", "Gotham-Light", 60, 0x4C4C4C, false);
			comeceTxt.alignPivot("center",VAlign.TOP);
			comeceTxt.x = stage.stageWidth / 2;
			comeceTxt.y = 400;
			comeceTxt.hAlign = "center";
			addChild(comeceTxt);
			
			btnIncio = new Button( Game.assets.getTexture("btnInicioBG"), "INICIAR");
			btnIncio.fontName = "Gotham-Light";
			btnIncio.fontSize = 40;
			btnIncio.fontColor = 0xFFFFFF;
			btnIncio.alignPivot("center",VAlign.TOP);
			btnIncio.x = stage.stageWidth / 2;
			btnIncio.y = 800;
			addChild(btnIncio);
			
			parceiro = new ParceiroHome();
			parceiro.alignPivot(HAlign.CENTER, VAlign.TOP);
			parceiro.x = stage.stageWidth / 2;
			parceiro.y = 1170;
			this.addChild(parceiro);
		}
		
		override public function transitionIn(e:Event=null):void
		{
			TweenMax.staggerFrom([ comeceTxt, btnIncio, parceiro], 0.3, {alpha:0}, 0.1, dispatchEventWith, [SceneEvent.TRANSITION_IN_COMPLETE]);
		}
		
		override public function transitionInComplete(e:Event=null):void
		{
			this.addEventListener(Event.TRIGGERED, onBtTrigger);
		}
		
		override public function transitionOutComplete(e:Event=null):void
		{
			this.removeEventListener(Event.TRIGGERED, onBtTrigger);
			
			var chs:Array = [];
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				chs.push( this.getChildAt(i) );
			}
			
			super.transitionOutComplete(e);
		}
		
		override public function transitionOut(e:Event=null):void
		{
			this.removeEventListener(Event.TRIGGERED, onBtTrigger);
			TweenMax.killTweensOf(parceiro);
			
			TweenMax.staggerTo([ comeceTxt, btnIncio, parceiro], 0.3, {alpha:0}, 0.1, dispatchEventWith, [SceneEvent.TRANSITION_OUT_COMPLETE]);
		}
		
		private function onBtTrigger(e:Event):void
		{
			var bt:Button = e.target as Button;
			if( bt == btnIncio ){
				btnIncio.enabled = false;
				btnIncio.alpha = 0.5;
				this.game.showScene(CPFScene);
			}
		}
	}
}

