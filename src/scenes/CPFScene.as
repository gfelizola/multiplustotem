package scenes
{
	import com.greensock.TweenMax;
	
	import events.SceneEvent;
	
	import game.NumPad;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.VAlign;
	
	import utils.Validator;

	public class CPFScene extends Scene
	{
		private var digiteTxt:TextField;
		
		private var inputBG:Image;
		private var cpfTxt:TextField;
		
		private var btnOk:Button;
		
		private var np:NumPad;
		private var cpfCV:int = 0;

		private var invalidoTxt:TextField;

		private var porFavorTxt:TextField;
		
		public function CPFScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event = null):void
		{
			this.y = -100;
			
			digiteTxt = new TextField(600, 150, "DIGITE SEU CPF\nPARA JOGAR", "Gotham-Light", 60, 0x4C4C4C, false);
			digiteTxt.alignPivot("center",VAlign.TOP);
			digiteTxt.x = stage.stageWidth / 2;;
			digiteTxt.y = 460;
			addChild(digiteTxt);
			
			inputBG = new Image( Game.assets.getTexture("inputsBG") );
			inputBG.alignPivot("center",VAlign.TOP);
			inputBG.x = stage.stageWidth / 2;
			inputBG.y = 790;
			addChild(inputBG);
			
			cpfTxt = new TextField(inputBG.width, inputBG.height, "___.___.___-__", "Gotham-Light", 58, 0xFFFFFF, false);
			cpfTxt.alignPivot("center",VAlign.TOP);
			cpfTxt.x = inputBG.x;
			cpfTxt.y = inputBG.y;
			cpfTxt.hAlign = "center";
			cpfTxt.vAlign = "center";
			addChild(cpfTxt);
			
			invalidoTxt = new TextField(inputBG.width, 60, "CPF inválido", "Gotham-Light", 46, 0xFFFFFF, false);
			invalidoTxt.alignPivot("center",VAlign.TOP);
			invalidoTxt.x = inputBG.x
			invalidoTxt.y = inputBG.y + 16;
			invalidoTxt.visible = false;
			addChild(invalidoTxt);
			
			porFavorTxt = new TextField(inputBG.width, 60, "por favor, digite novamente", "Gotham-Light", 28, 0xFFFFFF, false);
			porFavorTxt.alignPivot("center",VAlign.TOP);
			porFavorTxt.x = inputBG.x;
			porFavorTxt.y = inputBG.y + 60;
			porFavorTxt.visible = false;
			addChild(porFavorTxt);
			
			np = new NumPad();
			np.x = 430;
			np.y = 1000;
			np.addEventListener(NumPad.BUTTON_PRESS, onNumPadPress);
			addChild(np);
			
			btnOk = new Button(Game.assets.getTexture("btnOkBG"), "OK");
			btnOk.fontName = "Gotham-Light";
			btnOk.fontSize = 57;
			btnOk.fontColor = 0xFFFFFF;
			btnOk.alignPivot("center",VAlign.TOP);
			btnOk.x = stage.stageWidth / 2;
			btnOk.y = 1430;
			addChild(btnOk);
			
			this.addEventListener(Event.TRIGGERED, onBtTrigger);
			this.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function onBtTrigger(e:Event):void
		{
			var bt:Button = e.target as Button;
			if( bt == btnOk ){
				if( btnOk.text == "<" ){
					mostraCPF();
				} else {
					if( Validator.validateCPF(cpfTxt.text)){
						this.game.current.cpf = cpfTxt.text;
						this.game.showScene(InstrucoesScene);
					} else {
						mostraInvalido();
					}
				}
			}
		}
		
		private function mostraInvalido():void
		{
			cpfTxt.visible = false;
			invalidoTxt.visible = true;
			porFavorTxt.visible = true;
			btnOk.text = "<";
		}
		
		private function mostraCPF():void
		{
			cpfTxt.visible = true;
			invalidoTxt.visible = false;
			porFavorTxt.visible = false;
			btnOk.text = "OK";
		}
		
		private function onTouch(e:TouchEvent):void
		{
			if( invalidoTxt.visible ){
				if( 
					e.getTouch( inputBG, TouchPhase.ENDED ) ||
					e.getTouch( invalidoTxt, TouchPhase.ENDED ) ||
					e.getTouch( porFavorTxt, TouchPhase.ENDED ) ){
						mostraCPF();
				}
			}
		}
		
		private function onNumPadPress(e:Event):void
		{
			if( ! cpfTxt.visible ) return;
				
			var cpf:Array = cpfTxt.text.split("");
			var press:String = e.data as String;
			
			if( press.match(/[0-9]/) ){
				cpf[cpfCV > 13 ? 13 : cpfCV] = press;
				cpfTxt.text = cpf.join("");
				adiantaCursor();
				
			} else if( press == "<" ){
				cpf[voltaCursor()] = "_";
				cpfTxt.text = cpf.join("");
				
			} else if( press == "X" ){
				cpfCV=0;
				cpfTxt.text = "___.___.___-__";
			}
		}
		
		private function adiantaCursor():int
		{
			cpfCV++;
			if(cpfCV == 3 || cpfCV == 7 || cpfCV == 11 ) cpfCV++;
			if(cpfCV > 14 ) cpfCV = 14;
			return cpfCV;
		}
		
		private function voltaCursor():int
		{
			cpfCV--;
			if(cpfCV == 3 || cpfCV == 7 || cpfCV == 11 ) cpfCV--;
			if(cpfCV < 0 ) cpfCV = 0;
			return cpfCV;
		}
		
		override public function transitionIn(e:Event=null):void
		{
			TweenMax.staggerFrom([ digiteTxt, inputBG, cpfTxt, np, btnOk ], 1, {alpha:0}, 0.5, dispatchEventWith, [SceneEvent.TRANSITION_IN_COMPLETE]);
		}
		
		override public function transitionOut(e:Event=null):void
		{
			this.removeEventListener(Event.TRIGGERED, onBtTrigger);
			TweenMax.staggerTo([ btnOk, np, cpfTxt, inputBG, digiteTxt], 0.5, {alpha:0}, 0.1, dispatchEventWith, [SceneEvent.TRANSITION_OUT_COMPLETE]);
		}
	}
}