package scenes
{
	
	import com.greensock.TweenMax;
	
	import flash.display.BitmapData;
	import flash.display.Shape;
	
	import events.SceneEvent;
	
	import servico.Servico;
	import servico.ServicoRetorno;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	public class FeedbackScene extends Scene
	{
		private var parabens:TextField;
		private var ganhou:TextField;
		private var placar:TextField;
		private var pontos:TextField;
		private var multiplus:TextField;
		
		private var btnIncio:Button;
		
		private var linha1:Image;
		private var linha2:Image;
		private var linha3:Image;
		
		public function FeedbackScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event=null):void
		{
			super.create(e);
			
			parabens = new TextField(730, 200, "PARABÉNS!", "Gotham-Light", 118, 0x4C4C4C, false);
			parabens.hAlign = HAlign.CENTER;
			parabens.vAlign = VAlign.TOP;
			parabens.alignPivot(HAlign.CENTER, VAlign.TOP);
			parabens.x = stage.stageWidth / 2;
			parabens.y = 430;
			addChild(parabens);
			
			ganhou = new TextField(730, 100, "Você ganhou", "Gotham-Light", 84, 0x4C4C4C, false);
			ganhou.hAlign = HAlign.CENTER;
			ganhou.vAlign = VAlign.TOP;
			ganhou.alignPivot(HAlign.CENTER, VAlign.TOP);
			ganhou.x = stage.stageWidth / 2;
			ganhou.y = 570;
			addChild(ganhou);
			
			var pontosStr:String = this.game.current.pontuacao;
			while( pontosStr.length < 3 ) pontosStr = "0" + pontosStr;
			
			placar = new TextField(780, 700, this.game.current.pontuacao, "Gotham-Medium", 410, 0x4C4C4C, false);
			placar.hAlign = HAlign.CENTER;
			placar.vAlign = VAlign.TOP;
			placar.alignPivot(HAlign.CENTER, VAlign.TOP);
			placar.x = stage.stageWidth / 2;
			placar.y = 600;
			addChild(placar);
			
			pontos = new TextField(730, 300, "PONTOS", "Gotham-Light", 160, 0x4C4C4C, false);
			pontos.hAlign = HAlign.CENTER;
			pontos.vAlign = VAlign.TOP;
			pontos.alignPivot(HAlign.CENTER, VAlign.TOP);
			pontos.x = stage.stageWidth / 2;
			pontos.y = 1020;
			addChild(pontos);
			
			multiplus = new TextField(730, 300, "MULTIPLUS", "Gotham-Light", 120, 0x4C4C4C, false);
			multiplus.hAlign = HAlign.CENTER;
			multiplus.vAlign = VAlign.TOP;
			multiplus.alignPivot(HAlign.CENTER, VAlign.TOP);
			multiplus.x = stage.stageWidth / 2;
			multiplus.y = 1180;
			addChild(multiplus);
			
			linha1 = new Image(desenhaLinha(50));
			linha1.x = 200;
			linha1.y = 630;
			addChild(linha1);
			
			linha2 = new Image(desenhaLinha(50));
			linha2.x = 830;
			linha2.y = 630;
			addChild(linha2);
			
			linha3 = new Image(desenhaLinha(680));
			linha3.x = 200;
			linha3.y = 1330;
			addChild(linha3);
			
			btnIncio = new Button( Game.assets.getTexture("btnInicioBG"), "INÍCIO");
			btnIncio.fontName = "Gotham-Light";
			btnIncio.fontSize = 40;
			btnIncio.fontColor = 0xFFFFFF;
			btnIncio.alignPivot("center",VAlign.TOP);
			btnIncio.x = stage.stageWidth / 2;
			btnIncio.y = 1445;
			addChild(btnIncio);
			
			//envia dados para banco
			Servico.GravaParticipacaoOffLine( this.game.current.cpf, this.game.current.pontuacao, onGravou );
		}
		
		private function onGravou(sr:ServicoRetorno):void
		{
			if( sr.sucesso ){
				var protocolo:String = sr.retorno as String;
//				processo de impressão
				
//				Número do protocolo: <protocolo>
//				Data / Hora: new Date();
//				CPF: <cpf>
//				Qtd de pontos ganhos: <pontos>
//				Este é o seu comprovante de pontuação. Os pontos Multiplus serão creditados no CPF indicado em até 30 dias úteis, após a participação. Para participar do jogo e ganhar pontos Multiplus é obrigatório ser participante e ter o cadastro atualizado. Se não for participante consulte todas as regras e condições para acúmulo e resgate no site pontosmultiplus.com.br, e cadastre-se para ter direito aos pontos acumulados no jogo em até 15 dias após o termino da ação.
//				Em caso de dúvidas, ligue para 0300 313 7474. Permitido apenas uma participação por CPF, de XX/10/2014 a XX/12/2014. Pontuação máxima de até 160 pontos Multiplus. Ao final do jogo é informada a quantidades de pontos acumulados.
			}
		}
		
		private function desenhaLinha(w:int):Texture
		{
			var s:Shape = new Shape();
			s.graphics.lineStyle(2, 0x4C4C4C);
			s.graphics.lineTo(w,0);
			
			var bmd:BitmapData = new BitmapData(w, 2, false, 0x4C4C4C);
			bmd.draw(s);
			
			return Texture.fromBitmapData(bmd);
		}
		
		private function onBtTrigger(e:Event):void
		{
			var bt:Button = e.target as Button;
			if( bt == btnIncio ){
				btnIncio.enabled = false;
				btnIncio.alpha = 0.5;
				this.game.showScene(HomeScene);
			}
		}
		
		override public function transitionIn(e:Event=null):void
		{
			var chs:Array = [];
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				chs.push( this.getChildAt(i) );
			}
			
			TweenMax.staggerFrom(chs, 0.5, {alpha:0}, 0.2, dispatchEventWith, [SceneEvent.TRANSITION_IN_COMPLETE]);
		}
		
		override public function transitionInComplete(e:Event=null):void
		{
			this.addEventListener(Event.TRIGGERED, onBtTrigger);
			super.transitionInComplete(e);
		}
		
		
		override public function transitionOut(e:Event=null):void
		{
			this.removeEventListener(Event.TRIGGERED, onBtTrigger);
			
			var chs:Array = [];
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				chs.push( this.getChildAt(i) );
			}
			
			TweenMax.staggerTo(chs, 0.5, {alpha:0}, 0.05, dispatchEventWith, [SceneEvent.TRANSITION_OUT_COMPLETE]);
		}
		
		override public function transitionOutComplete(e:Event=null):void
		{
			while(this.numChildren){
				this.removeChildAt(0, true);
			}
			
			super.transitionOutComplete(e);
		}
		
		
	}
}