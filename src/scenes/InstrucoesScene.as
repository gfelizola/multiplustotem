package scenes
{
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	import com.greensock.easing.Power2;
	
	import events.SceneEvent;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import utils.Validator;

	public class InstrucoesScene extends Scene
	{
		private var fundo:Image;
		private var star1:Image;
		private var star2:Image;
		private var hand:Image;
		
		private var encontre:TextField;
		private var quanto:TextField;
		private var btnJogar:Button;

		private var tl:TimelineMax;
		
		public function InstrucoesScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event=null):void
		{
			super.create(e);
			
			encontre = new TextField(stage.stageWidth, 200, "Encontre os logos iguais\no mais rápido possível.", "Gotham-Light", 56, 0x4C4C4C, false);
			encontre.hAlign = HAlign.CENTER;
			encontre.alignPivot(HAlign.CENTER, VAlign.TOP);
			encontre.x = stage.stageWidth / 2;
			encontre.y = 400;
			addChild(encontre);
			
			quanto = new TextField(stage.stageWidth, 200, "Quanto mais pares você fizer,\nmais pontos vai ganhar.", "Gotham-Light", 56, 0x4C4C4C, false);
			quanto.hAlign = HAlign.CENTER;
			quanto.alignPivot(HAlign.CENTER, VAlign.TOP);
			quanto.x = stage.stageWidth / 2;
			quanto.y = 400;
			quanto.alpha = 0;
			addChild(quanto);
			
			fundo = new Image( Game.assets.getTexture("instrucoesBolas") );
			fundo.alignPivot(HAlign.CENTER, VAlign.TOP);
			fundo.x = stage.stageWidth / 2;
			fundo.y = 700;
			addChild(fundo);
			
			btnJogar = new Button( Game.assets.getTexture("btnJogarBG"), "JOGAR!");
			btnJogar.fontName = "Gotham-Light";
			btnJogar.fontSize = 56;
			btnJogar.fontColor = 0xFFFFFF;
			btnJogar.alignPivot("center",VAlign.TOP);
			btnJogar.x = stage.stageWidth / 2;
			btnJogar.y = 1400;
			addChild(btnJogar);
			
			star1 = new Image( Game.assets.getTexture("instrucoesStar") );
			star1.x = 415;
			star1.y = 1026;
			star1.alpha = 0;
			addChild(star1);
			
			star2 = new Image( Game.assets.getTexture("instrucoesStar") );
			star2.x = 560;
			star2.y = 876;
			star2.alpha = 0;
			addChild(star2);
			
			hand = new Image( Game.assets.getTexture("instrucoesHand") );
			hand.x = 460;
			hand.y = 1070;
			hand.alpha = 0;
			addChild(hand);
		}
		
		override public function transitionIn(e:Event=null):void
		{
			TweenMax.staggerFrom([ encontre, fundo, btnJogar ], 0.5, {alpha:0}, 0.5, iniciarInstrucoes);
		}
		
		override public function transitionOut(e:Event=null):void
		{
			tl.stop();
			this.removeEventListener(Event.TRIGGERED, onBtTrigger);
			TweenMax.staggerTo([ encontre, quanto, star1, star2, hand, fundo, btnJogar ], 0.5, {alpha:0}, 0.1, dispatchEventWith, [SceneEvent.TRANSITION_OUT_COMPLETE]);
		}
		
		private function onBtTrigger(e:Event):void
		{
			var bt:Button = e.target as Button;
			if( bt == btnJogar ){
				btnJogar.enabled = false;
				btnJogar.alpha = 0.5;
				this.game.showScene(JogoScene);
			}
		}
		
		private function iniciarInstrucoes():void
		{
			this.addEventListener(Event.TRIGGERED, onBtTrigger);
			
			tl = new TimelineMax({repeat:-1, repeatDelay:1});
			tl.set( hand, {x:467, y:1070});
			tl.to( hand, 0.8, {alpha:1});
			tl.to( hand, 0.1, {scaleX:0.9, scaleY:0.9, delay:0.3});
			tl.to( hand, 0.1, {scaleX:1,   scaleY:1 });
			tl.to( star1, 0.5, {alpha:1});
			tl.to( hand, 0.5, {x:615, y:920, ease:Power2.easeOut});
			tl.to( hand, 0.1, {scaleX:0.9, scaleY:0.9, delay:0.3});
			tl.to( hand, 0.1, {scaleX:1,   scaleY:1 });
			tl.to( star2, 0.5, {alpha:1});
			tl.to( encontre, 0.5, {alpha:0});
			tl.to( quanto, 0.5, {alpha:1});
			
			tl.staggerTo([star1, star2, hand, quanto], 0.5, {alpha:0, delay:2}, 0.2);
			tl.to( encontre, 0.5, {alpha:1});
			
		}
	}
}