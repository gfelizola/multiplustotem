package scenes
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	import flash.text.SoftKeyboardType;
	import flash.text.StageText;
	import flash.text.TextFormatAlign;
	
	import game.NumPad;
	
	import servico.Servico;
	import servico.ServicoRetorno;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.VAlign;
	
	import utils.ProgressBar;

	public class CopiarScene extends Scene
	{
		public static const NOME_SHARED:String = "MultiplusMemoria";
		
		private var btnIncio:Button;
		private var btnVoltar:Button;
		private var btnTempo:Button;
		
		private var inputBG:Image;
		
		private var _progress:ProgressBar;
		private var tempoTxt:TextField;
		
		private var np:NumPad;
		
		
		public function CopiarScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event=null):void
		{
			var txt1:TextField = new TextField(600, 150, "Aperte em INICIAR para\nenviar as informações", "Gotham-Light", 24, 0x4C4C4C, false);
			txt1.alignPivot("center",VAlign.TOP);
			txt1.x = stage.stageWidth / 2;;
			txt1.y = 1000;
			addChild(txt1);
			
			btnIncio = new Button( Game.assets.getTexture("btnInicioBG"), "INICIAR");
			btnIncio.fontName = "Gotham-Light";
			btnIncio.fontSize = 40;
			btnIncio.fontColor = 0xFFFFFF;
			btnIncio.alignPivot("center",VAlign.TOP);
			btnIncio.x = stage.stageWidth / 2;
			btnIncio.y = 1220;
			addChild(btnIncio);
			
			btnVoltar = new Button( Game.assets.getTexture("btnInicioBG"), "VOLTAR");
			btnVoltar.fontName = "Gotham-Light";
			btnVoltar.fontSize = 40;
			btnVoltar.fontColor = 0xFFFFFF;
			btnVoltar.alignPivot("center",VAlign.TOP);
			btnVoltar.x = stage.stageWidth / 2;
			btnVoltar.y = 1500;
			addChild(btnVoltar);
			
			_progress = new ProgressBar(400, 20);
			_progress.x = (stage.stageWidth-400) / 2;
			_progress.y = 1150;
			_progress.ratio = 0;
			addChild(_progress);
			
			
			
			var so:SharedObject = SharedObject.getLocal(CopiarScene.NOME_SHARED);
			var tempoGravado:int = so.data.tempoGravado;
			
			var txt2:TextField = new TextField(600, 150, "Altere o valor e clique em OK para alterar o tempo do jogo", "Gotham-Light", 24, 0x4C4C4C, false);
			txt2.alignPivot("center",VAlign.TOP);
			txt2.x = stage.stageWidth / 2;
			txt2.y = 130;
			addChild(txt2);
			
			inputBG = new Image( Game.assets.getTexture("btnInicioBG") );
			inputBG.alignPivot("center",VAlign.TOP);
			inputBG.x = (stage.stageWidth / 2) - 100;
			inputBG.y = 300;
			addChild(inputBG);
			
			if( isNaN(tempoGravado) ) tempoGravado = 20;
			
			tempoTxt = new TextField(inputBG.width, inputBG.height, tempoGravado.toString(), "Gotham-Light", 58, 0xFFFFFF, false);
			tempoTxt.alignPivot("center",VAlign.TOP);
			tempoTxt.x = inputBG.x;
			tempoTxt.y = inputBG.y;
			tempoTxt.hAlign = "center";
			tempoTxt.vAlign = "center";
			addChild(tempoTxt);
			
			btnTempo = new Button(Game.assets.getTexture("btnOkBG"), "OK");
			btnTempo.fontName = "Gotham-Light";
			btnTempo.fontSize = 34;
			btnTempo.fontColor = 0xFFFFFF;
			btnTempo.alignPivot("center",VAlign.TOP);
			btnTempo.x = inputBG.x + inputBG.width - 20;
			btnTempo.y = 280;
			addChild(btnTempo);
			
			np = new NumPad();
			np.x = 430;
			np.y = 450;
			np.addEventListener(NumPad.BUTTON_PRESS, onNumPadPress);
			addChild(np);
			
			this.addEventListener(Event.TRIGGERED, onTrigger);
			
			super.create(e);
		}
		
		private function onTrigger(e:Event):void
		{
			if( e.target == btnIncio ){
				iniciarCopia();
			} else if( e.target == btnVoltar ){
				btnIncio.enabled = false;
				btnVoltar.enabled = false;
				
				btnIncio.alpha = 0.5;
				btnVoltar.alpha = 0.5;
				
				this.game.showScene(HomeScene);
			}
			else if( e.target == btnTempo )
			{
				var tempo:int = parseInt( tempoTxt.text ); 
				var so:SharedObject = SharedObject.getLocal(CopiarScene.NOME_SHARED);
				so.data.tempoGravado = tempo;
				so.flush();
				
				trace("salvou tempo", tempo);
				
				this.game.tempo = tempo;
			}
		}
		
		private function onNumPadPress(e:Event):void
		{
			if( ! tempoTxt.visible ) return;
			
			var press:String = e.data as String;
			
			if( press.match(/[0-9]/) ){
				tempoTxt.text += press;
				trace( press, tempoTxt.text );
			} else if( press == "<" ){
				if( tempoTxt.text.length > 1 ){
					tempoTxt.text = tempoTxt.text.substr(0, tempoTxt.text.length - 1);
				} else {
					tempoTxt.text = "";
				}
			}
		}
		
		private function iniciarCopia():void
		{
			btnIncio.enabled = false;
			btnVoltar.enabled = false;
			
			btnIncio.alpha = 0.5;
			btnVoltar.alpha = 0.5;
			
			var obj:Object = { ratio:0 };
			TweenMax.to( obj, 10, { ratio:1, ease:Linear.easeNone, onUpdate:function():void{
				_progress.ratio = obj.ratio;
			}});
			
			Servico.CopiaArquivosPenDrive(onCopiou);
		}
		
		private function onCopiou(sr:ServicoRetorno = null):void
		{
			trace(sr);
			
			TweenMax.killAll();
			
			btnIncio.enabled = true;
			btnVoltar.enabled = true;
			
			btnIncio.alpha = 1;
			btnVoltar.alpha = 1;
			_progress.ratio = 1;
		}
		
		override public function transitionIn(e:Event=null):void
		{
			super.transitionIn(e);
		}
		
		override public function transitionInComplete(e:Event=null):void
		{
			// TODO Auto Generated method stub
			super.transitionInComplete(e);
		}
		
		override public function transitionOut(e:Event=null):void
		{
			// TODO Auto Generated method stub
			super.transitionOut(e);
		}
		
		override public function transitionOutComplete(e:Event=null):void
		{
			// TODO Auto Generated method stub
			super.transitionOutComplete(e);
		}
		
		
	}
}