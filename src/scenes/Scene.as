package scenes
{
    import events.SceneEvent;
    
    import starling.display.Sprite;
    import starling.events.Event;
    
    public class Scene extends Sprite
    {
		public var game:Game;
		
        public function Scene(g:Game)
        {
			this.game = g;
			
			if( stage ) create();
			else addEventListener(Event.ADDED_TO_STAGE, create);
			
            this.addEventListener(SceneEvent.TRANSITION_IN_START, transitionIn);
            this.addEventListener(SceneEvent.TRANSITION_IN_COMPLETE, transitionInComplete);
            this.addEventListener(SceneEvent.TRANSITION_IN_START, transitionOut);
            this.addEventListener(SceneEvent.TRANSITION_OUT_COMPLETE, transitionOutComplete);
        }
		
		public function create(e:Event = null):void
		{
			
		}
		
		public function transitionIn(e:Event = null):void
		{
			dispatchEventWith(SceneEvent.TRANSITION_IN_COMPLETE, true);
		}
		
		public function transitionInComplete(e:Event = null):void
		{
			trace("Scene transition in complete");
		}
		
		public function transitionOut(e:Event = null):void
		{
			dispatchEventWith(SceneEvent.TRANSITION_OUT_COMPLETE, true);
		}
		
		public function transitionOutComplete(e:Event = null):void
		{
			while(this.numChildren){
				this.removeChildAt(0, true);
			}
			
			dispose();
		}
    }
}