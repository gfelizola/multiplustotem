package utils
{
	/**
	 * @author Timo
	 */
	public class Validator
	{
		public static const DOT_DOT:String = "dotDot";
		public static const DOT_DASH:String = "dotDash";
		public static const DOT_SPACE:String = "dotSpace";
		public static const DASH_DASH:String = "dashDash";
		public static const DASH_DOT:String = "dashDot";
		public static const DASH_SPACE:String = "dashSpace";
		public static const SPACE_SPACE:String = "spaceSpace";
		public static const SPACE_DOT:String = "spaceDot";
		public static const SPACE_DASH:String = "spaceDash";
		public static const NONE:String = "none";
		
		/**
		 * Valida um Email. O id do email pode ter caracteres alphanumÃ©ricos e mais os seguintes (opicionais):
		 * '-', '.' e '_'.
		 * @param email A String do email.
		 * @return Retorna true se o email Ã© vÃ¡lido, false se nÃ£o.
		 */
		public static function validateEmail(email:String):Boolean
		{
			var emailIdRegex:RegExp = /^[a-z0-9]+([._][a-z0-9]+)*@/i;
			if(!emailIdRegex.test(email))
			{
				return false;
			}
			
			var emailDomain:String = email.replace(emailIdRegex, "");
			if(!Validator.validateDomain(emailDomain))
			{
				return false;
			}
			return true;
		}
		
		/**
		 * Valida um domÃ­nio limpo, ou seja, sem 'www' e sem '/' no final. Exemplo: 'google.com.br'
		 * @param domain A String do domÃ­nio.
		 * @return Retorna true se o domÃ­nio Ã© vÃ¡lido, false se nÃ£o.
		 */
		public static function validateDomain(domain:String):Boolean
		{
			var domainRegex:RegExp = /^[a-z0-9][-a-z0-9]*(\.[a-z]{2,6}){1,2}$/i;
			if(!domainRegex.test(domain))
			{
				return false;
			}
			return true;
		}
		
		/**
		 * Valida DDD de 2 dÃ­gitos com um zero opcional no comeÃ§o:
		 * @param ddd A String do DDD.
		 * @return Retorna true se o DDD Ã© vÃ¡lido, false se nÃ£o.
		 */
		public static function validateDDD(ddd:String):Boolean
		{
			var dddRegex:RegExp = /^0?\d{2}$/;
			if(!dddRegex.test(ddd))
			{
				return false;
			}
			return true;
		}
		
		/**
		 * Valida um telefone contendo 8 dÃ­gitos e um dos seguintes separadores (opcionais):
		 * '-' e ' '.
		 * @param phone A String do Telefone.
		 * @return Retorna true se o Telefone Ã© vÃ¡lido, false se nÃ£o.
		 */
		public static function validatePhone(phone:String):Boolean
		{
			var phoneRegex:RegExp = /^\d{4}[-\s]?\d{4}$/;
			if(!phoneRegex.test(phone))
			{
				return false;
			}
			return true;
		}
		
		/**
		 * Valida um telefone celular contendo 8 dÃ­gitos e um dos seguintes separadores (opcionais):
		 * '-' e ' '.
		 * @param cellphone A String do telefone celular.
		 * @return Retorna true se o telefone celular Ã© vÃ¡lido, false se nÃ£o.
		 */
		public static function validateCellphone(cellphone:String):Boolean
		{
			var phoneRegex:RegExp = /^[6-9]\d{3}[-\s]?\d{4}$/;
			if(!phoneRegex.test(cellphone))
			{
				return false;
			}
			return true;
		}
		
		/**
		 * Valida um CEP Brasileiro contendo 8 dÃ­gitos e um dos seguintes separadores (opcionais):
		 * '.', '-' e ' '.
		 * @param cep A String do CEP.
		 * @return Retorna true se o CEP Ã© vÃ¡lido, false se nÃ£o.
		 */
		public static function validateCEPBrasileiro(cep:String):Boolean
		{
			var cepRegex:RegExp = /^\d{2}[-.\s]?\d{3}[-.\s]?\d{3}$/;
			if(!cepRegex.test(cep))
			{
				return false;
			}
			return true;
		}
		
		/**
		 * Valida um CPF contendo nÃºmeros e os seguintes separadores (opcionais):
		 * '-', '.' e ' '.
		 * @param cpf A String do CPF.
		 * @return Retorna true se o CPF Ã© vÃ¡lido, false se nÃ£o.
		 */
		public static function validateCPF(cpf:String):Boolean
		{
			var cpfRegex:RegExp = /^\d{3}[-. ]?\d{3}[-. ]?\d{3}[-. ]?\d{2}$/;
			if(!cpfRegex.test(cpf))
			{
				return false;
			}
			
			var cc:String = Validator.replaceString(cpf, new RegExp(/[-. ]/));
			
			for (var i:int = 0; i < 10; i++) 
			{
				var uni:String = "";
				for (var j:int = 0; j < 11; j++) 
				{
					uni += i + "";
				}
				
				if( cc == uni ) return false;
			}
			
			
			var firstDigit:Number = 0;
			var secondDigit:Number = 0;
			
			var firstSum:Number = Validator.makeSum(cc.substr(0, 9));
			var firstRest:Number = firstSum % 11;
			if(firstRest >= 2)
			{
				firstDigit = 11 - firstRest;
			}
			
			var secondSum:Number = Validator.makeSum(cc.substr(0, 9), 11);
			secondSum += firstDigit * 2;
			var secondRest:Number = secondSum % 11;
			if(secondRest >= 2)
			{
				secondDigit = 11 - secondRest;
			}
			
			if(Number(cc.charAt(9)) == firstDigit && Number(cc.charAt(10)) == secondDigit)
			{
				return true;
			}
			return false;
		}
		
		/**
		 * Gera um CPF vÃ¡lido.
		 * @param caracterSeparation Uma string que diz por que caracteres o CPF retornado vai ser separado. PossÃ­veis valores sÃ£o as constantes desta classe.
		 * @return o CPF gerado.
		 */
		public static function generateValidCPF(caracterSeparation:String = "dotDash"):String
		{
			var cc:String = "";
			for(var i:uint = 0; i < 9; i++)
			{
				cc += String(Math.round(Math.random() * 9));
			}
			
			var firstDigit:Number = 0;
			var secondDigit:Number = 0;
			
			var firstSum:Number = Validator.makeSum(cc);
			var firstRest:Number = firstSum % 11;
			if(firstRest >= 2)
			{
				firstDigit = 11 - firstRest;
			}
			
			var secondSum:Number = Validator.makeSum(cc, 11);
			secondSum += firstDigit * 2;
			var secondRest:Number = secondSum % 11;
			if(secondRest >= 2)
			{
				secondDigit = 11 - secondRest;
			}
			cc += String(firstDigit) + String(secondDigit);
			
			switch(caracterSeparation)
			{
				case DOT_DOT :
					cc = Validator.putCaracters(cc, ".", ".", ".");
					break;
				case DOT_DASH :
					cc = Validator.putCaracters(cc, ".", ".", "-");
					break;
				case DOT_SPACE :
					cc = Validator.putCaracters(cc, ".", ".", " ");
					break;
				case DASH_DASH :
					cc = Validator.putCaracters(cc, "-", "-", "-");
					break;
				case DASH_DOT :
					cc = Validator.putCaracters(cc, "-", "-", ".");
					break;
				case DASH_SPACE :
					cc = Validator.putCaracters(cc, "-", "-", " ");
					break;
				case SPACE_SPACE :
					cc = Validator.putCaracters(cc, " ", " ", " ");
					break;
				case SPACE_DOT :
					cc = Validator.putCaracters(cc, " ", " ", ".");
					break;
				case SPACE_DASH :
					cc = Validator.putCaracters(cc, " ", " ", "-");
					break;
				default :
					break;
			}
			
			return cc;
		}
		
		/**
		 * Valida um CNPJ contendo nÃºmeros e os seguintes separadores (opcionais):
		 * '-', '.', '/' (antes dos Ãºltimos 6 caracteres) e ' '.
		 * @param cnpj A String do CNPJ.
		 * @return Retorna true se o CNPJ Ã© vÃ¡lido, false se nÃ£o.
		 */
		public static function validateCNPJ(cnpj:String):Boolean
		{
			var cnpjRegex:RegExp = /^\d{2}[-. \/]?\d{3}[-. \/]?\d{3}[-. \/]?\d{4}[-. \/]?\d{2}$/;
			if(!cnpjRegex.test(cnpj))
			{
				return false;
			}
			
			var cc:String = Validator.replaceString(cnpj, new RegExp(/[-. \/]/));
			var firstDigit:Number = 0;
			var secondDigit:Number = 0;
			
			var firstSum:Number = Validator.makeSum(cc.substr(0, 4), 5) + Validator.makeSum(cc.substr(4, 8), 9);
			var firstRest:Number = firstSum % 11;
			if(firstRest >= 2)
			{
				firstDigit = 11 - firstRest;
			}
			
			var secondSum:Number = Validator.makeSum(cc.substr(0, 5), 6) + Validator.makeSum(cc.substr(5, 7), 9);
			secondSum += firstDigit * 2;
			var secondRest:Number = secondSum % 11;
			if(secondRest >= 2)
			{
				secondDigit = 11 - secondRest;
			}
			if(Number(cc.charAt(12)) == firstDigit && Number(cc.charAt(13)) == secondDigit)
			{
				return true;
			}
			return false;
		}
		/**
		 * Gera um CNPJ vÃ¡lido.
		 * @param includePunctuation Um Boolean que define se o CNPJ gerado vai conter a pontuaÃ§Ã£o padrao (00.000.000/0000-00). Coloque false se quiser apenas o nÃºmero.
		 * @return o CNPJ gerado.
		 */
		public static function generateValidCNPJ(includePunctuation:Boolean = true):String
		{
			var cc:String = "";
			for(var i:uint = 0; i < 8; i++)
			{
				cc += String(Math.round(Math.random() * 9));
			}
			cc += "0001";
			
			var firstDigit:Number = 0;
			var secondDigit:Number = 0;
			
			var firstSum:Number = Validator.makeSum(cc.substr(0, 4), 5) + Validator.makeSum(cc.substr(4, 8), 9);
			var firstRest:Number = firstSum % 11;
			if(firstRest >= 2)
			{
				firstDigit = 11 - firstRest;
			}
			
			var secondSum:Number = Validator.makeSum(cc.substr(0, 5), 6) + Validator.makeSum(cc.substr(5, 7), 9);
			secondSum += firstDigit * 2;
			var secondRest:Number = secondSum % 11;
			if(secondRest >= 2)
			{
				secondDigit = 11 - secondRest;
			}
			
			cc += String(firstDigit) + String(secondDigit);
			if(includePunctuation)
			{
				cc = cc.substr(0, 2) + "." + cc.substr(2, 3) + "." + cc.substr(5, 3) + "/" + cc.substr(8, 4) + "-" + cc.substr(12);
			}
			
			return cc;
		}
		
		/**
		 * Limpa (deixa apenas os nÃºmeros, Ãºtil para guardar num banco de dados) uma string contendo nÃºmeros e os seguintes caracteres (opcionais):
		 * '-', '.', ' ', '_', '/', '(' e ')'.
		 * @param string A String a ser limpada.
		 * @return Retorna a string limpa convertida em Number
		 */
		public static function cleanNumberString(string:String):Number
		{
			var stringChecker:RegExp = /^\d+$/;
			var cs:String = Validator.replaceString(string, new RegExp(/[-. _()\/]/));
			if(!stringChecker.test(cs))
			{
				throw new Error("Sua string contÃ©m caracteres irreconhecidos pela funÃ§Ã£o. Os caracteres reconhecidos sÃ£o: '-', '.', ' ', '_', '(' e ')'.");
			}
			return Number(cs);
		}
		
		/**
		 * Checa se uma string contÃ©m apenas caracteres numÃ©ricos
		 * @param string A String a ser checada.
		 * @return Retorna true se a string contÃ©m apenas caracteres numÃ©ricos, false se nÃ£o.
		 */
		public static function isNumber(string:String):Boolean
		{
			var digitChecker:RegExp = /^\d+$/;
			if(!digitChecker.test(string))
			{
				return false;
			}
			return true;
		}
		
		/**
		 * Checa se uma string contÃ©m apenas caracteres alfabÃ©ticos e/ou numÃ©ricos
		 * @param string A String a ser checada.
		 * @return Retorna true se a string contÃ©m apenas caracteres alfabÃ©ticos e/ou numÃ©ricos, false se nÃ£o.
		 */
		public static function isAlphaNumeric(string:String):Boolean
		{
			var caracterChecker:RegExp = /^\w+$/;
			if(caracterChecker.test(string) && string.search("_") === -1)
			{
				return true;
			}
			return false;
		}
		
		/**
		 * Checa se uma string contÃ©m um mÃ­nimo de min caracteres e um mÃ¡ximo de max caracteres
		 * @param string A String a ser checada.
		 * @param min O mÃ­nimmo de caracteres.
		 * @param max O mÃ¡ximo de caracteres.
		 * @return Retorna true se a string um nÃºmero de caracteres entre min e max, false se nÃ£o.
		 */
		public static function minMax(string:String, min:uint = 1, max:uint = 30):Boolean
		{
			if(string.length < min || string.length > max)
			{
				return false;
			}
			return true;
		}
		
		private static function makeSum(numClean:String, startAt:uint = 10):Number
		{
			var returnVal:Number = 0;
			for(var i:uint = 0; i < numClean.length; i++)
			{
				returnVal += Number(numClean.charAt(i)) * (startAt - i);
			}
			return returnVal;
		}
		
		private static function replaceString(stringToReplace:String, replacePattern:RegExp, replacePatternsByThis:String = "", maxReplaces:uint = 200):String
		{
			for(var i:uint = 0; i < maxReplaces; i++)
			{
				if(stringToReplace.search(replacePattern) != -1)
				{
					stringToReplace = stringToReplace.replace(replacePattern, replacePatternsByThis);
				}
				if(stringToReplace.search(replacePattern) == -1)
				{
					break;
				}
			}
			return stringToReplace;
		}
		
		private static function putCaracters(string:String, fc:String, sc:String, tc:String):String
		{
			return string.substr(0, 3) + fc + string.substr(3, 3) + sc + string.substr(6, 3) + tc + string.substr(9, 2);
		}
	}
}