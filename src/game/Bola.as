package game
{
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.Shape;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class Bola extends Sprite
	{
		public var parceiro:String = "";
		
		private var _aberta:Boolean = false;

		private var viraTL:TimelineMax;
		
		public function Bola()
		{
			super();
		}
		
		public function criar(txName:String, cor:uint, radius:Number):void
		{
			var frente:MovieClip = new MovieClip( Game.assets.getTextureAtlas("bola" + cor).getTextures(), 30);
			frente.alignPivot(HAlign.CENTER, VAlign.CENTER);
			frente.width = frente.height = radius;
			frente.x = 0;
			frente.y = 0;
			frente.name = "frente";
			frente.stop();
			
			var tras:MovieClip = new MovieClip( Game.assets.getTextureAtlas(txName).getTextures(), 30);
			tras.alignPivot(HAlign.CENTER, VAlign.CENTER);
			tras.width = tras.height = radius;
			tras.alpha = 0;
			tras.x = 0;
			tras.y = 0;
			tras.name = "tras";
			tras.stop();
				
			Starling.juggler.add(frente);
			Starling.juggler.add(tras);
			
			addChild(frente);
			addChild(tras);
			
			var obj1:Object = {frame:0};
			var obj2:Object = {frame:0};
			
			viraTL = new TimelineMax({paused:true});
			viraTL.to( obj1, frente.numFrames / 30, {frame: frente.numFrames - 1, ease:Linear.easeNone, onUpdate:atualizaFrame, onUpdateParams:[obj1, frente]});
			viraTL.set( frente, { alpha:0 });
			viraTL.set( tras, { alpha:1 });
			viraTL.to( obj2, tras.numFrames / 30, {frame: tras.numFrames - 1, ease:Linear.easeNone, onUpdate:atualizaFrame, onUpdateParams:[obj2, tras]});
			
			parceiro = txName.replace("parceiro-","").replace("-ani","");
		}
		
		public function abrir():void
		{
			viraTL.play();
		}
		
		public function fechar():void
		{
			viraTL.reverse();
		}
		
		private function atualizaFrame(framer:Object, mc:MovieClip):void
		{
			mc.currentFrame = Math.round( framer.frame );
		}
	}
}