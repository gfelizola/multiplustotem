package
{
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.system.Capabilities;
	
	import adobe.utils.CustomActions;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.utils.AssetManager;
	
	[SWF(width="1080", height="1920", frameRate="30", backgroundColor="#ffffff")]
	public class MultiplusGameTotem extends Sprite
	{
		private var mStarling:Starling;
		
		public function MultiplusGameTotem()
		{
			if (stage) start();
			else addEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function start():void
		{
			stage.displayState = StageDisplayState.FULL_SCREEN;
			
			Starling.multitouchEnabled = false;
			Starling.handleLostContext = true;
			
			mStarling = new Starling(Game, stage);
			mStarling.enableErrorChecking = Capabilities.isDebugger;
			mStarling.simulateMultitouch = false;
			mStarling.start();
			
			trace("View", stage.stageWidth, stage.stageHeight);
			
			stage.addEventListener(flash.events.Event.RESIZE, stageResize);
			mStarling.addEventListener(starling.events.Event.ROOT_CREATED, onRootCreated);
			
			trace( File.applicationDirectory.resolvePath("").url );
		}
		
		protected function stageResize(event:flash.events.Event):void
		{
			trace("View", stage.stageWidth, stage.stageHeight);
		}
		
		private function onAddedToStage(event:Object):void
		{
			removeEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStage);
			start();
		}
		
		private function onRootCreated(event:starling.events.Event, game:Game):void
		{
			if (mStarling.context.driverInfo.toLowerCase().indexOf("software") != -1)
				mStarling.nativeStage.frameRate = 30;
			
			var assets:AssetManager = new AssetManager();
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue( File.applicationDirectory.resolvePath("textures") );
			game.start(assets);
		}
	}
}